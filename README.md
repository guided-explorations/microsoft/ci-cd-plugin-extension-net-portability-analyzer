## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/ci-cd-plugin-extension-net-portability-analyzer)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## CI CD Plugin Extension 

This guided exploration implements CI CD library code in the form of a plugin extension.  It also models the general concept of building a standardized, version controlled plugin extension in GitLab.

## Not A Production Solution

The following disclaimer is emited to the logs by this plugin.  When using it in your own projects, you must copy the repository or it's files and then you can remove the code that emits this statement.

"The original location for this plugin extension is for demonstration and learning purposes only.  There is no breaking change SLA and it may change at any time without notice. If you need to depend on it for production or other critical systems, please make your own isolated copy to depend on.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-02-26

* **GitLab Version Released On**: 12.6

* **GitLab Edition Required**: 

  * For overall solution: [![FC](../config-data-monorepo/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:

## Demonstrates These Design Requirements and Desirements

As a complete whole, this guided exploration requires at least the Free / Core (![FC](images/FC.png)) edition of GitLab.

- **Development Pattern:** a standard CI CD Plugin Extension that can be version controlled in one repository and used in any .gitlab-ci.yml anywhere on the planet.
- **GitLab Development Pattern:** a plugin extension that consists of an includable GitLab CI yml job that is standardized and reusable.

#### GitLab CI Functionality:

- **.gitlab-ci.yml** "includes" ![FC](images/FC.png)can be a file in the same repo, another repo or an https url
  - Used to enable manage main build templates (gitlab ci custom functions) to have more change controls over them.
- **.gitlab-ci.yml** "extends" ![FC](images/FC.png)parameter for "template jobs" (gitlab ci custom functions).

## Using This Pattern:
- This repository contains a GitLab CI file showing the use of the plugin: [calling-plugin-sample.yml](./calling-plugin-sample.yml)
- The Guided Exploration [.NET API Portability Analyzer](https://gitlab.com/guided-explorations/net-framework/gitlab-ci-job-for-.net-api-portability-analyzer) is a working pattern that implements this pattern.
- The comments in [ci-cd-plugin-extension-net-portability-analyzer.yml](ci-cd-plugin-extension-net-portability-analyzer.yml) provide additional guidance on usage.

## Documentation for the Underlying Software:

IMPORTANT: Since this is static assembly analysis, the assemblies you are analyzing DO NOT have to be runnable on this Linux .NET Core container.

Documentation Links:
*  Excellent .NET Core Refactor Project Plan: https://www.xoriant.com/blog/product-engineering/still-thinking-porting-net-net-core.html
*  https://jmezach.github.io/2018/11/07/porting-from-asp.net-to-asp.net-core-part-2-third-party-dependencies/
*  https://social.technet.microsoft.com/wiki/contents/articles/36600.checking-compatibility-with-net-portability-analyzer.aspx
*  https://channel9.msdn.com/Blogs/Seth-Juarez/A-Brief-Look-at-the-NET-Portability-Analyzer
*  https://www.linkedin.com/learning/navigating-dot-net-and-dot-net-standard-for-cross-platform-development/using-the-dot-net-portability-analyzer-with-visual-studio

